# NYC Schools

NYC Schools is an iOS application that displays information about schools in New York City. The app fetches data from two API endpoints and presents them in a user-friendly interface.

# Features

- Display a list of schools in New York City
- Display school details, including name, overview, phone number, email, website, extracurricular activities, city, zip, final grades, total students, school sports, reading score, math score, and writing score.

# Tech Stack

- Swift programming language
- Combine framework for handling asynchronous requests
- UIKit for building the user interface
- XCTest for unit testing
