//
//  SchoolInfoTests.swift
//  NYCSchoolsTests
//
//  Created by Sravanthi Kande on 5/17/23.
//

import XCTest
@testable import NYCSchools

class SchoolInfoTests: XCTestCase {
    
    func testDecodingSchoolInfo() {
        
        guard let schoolInfo = MockSchoolService.mockResponse.first else {
            return XCTFail("schoolInfo should not be nil")
        }
        
        XCTAssertEqual(schoolInfo.dbn, "01M292")
        XCTAssertEqual(schoolInfo.schoolName, "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES")
        XCTAssertEqual(schoolInfo.overviewParagraph, "Henry Street School for International Studies is a small college preparatory school located in Manhattan.")
        XCTAssertEqual(schoolInfo.phoneNumber, "212-406-9411")
        XCTAssertNil(schoolInfo.schoolEmail)
        XCTAssertEqual(schoolInfo.website, "http://www.schools.nyc.gov/schoolportals/02/M292")
        XCTAssertEqual(schoolInfo.extracurricularActivities, "School Based Leadership Team, Drama Club, Student Council")
        XCTAssertEqual(schoolInfo.city, "New York")
        XCTAssertEqual(schoolInfo.zip, "10002")
        XCTAssertEqual(schoolInfo.finalgrades, "6-12")
        XCTAssertEqual(schoolInfo.totalStudents, "323")
        XCTAssertEqual(schoolInfo.schoolSports, "Basketball, Softball, Volleyball")
        XCTAssertEqual(schoolInfo.readingScore, "355")
        XCTAssertEqual(schoolInfo.mathScore, "404")
        XCTAssertEqual(schoolInfo.writingScore, "363")
    }
}
