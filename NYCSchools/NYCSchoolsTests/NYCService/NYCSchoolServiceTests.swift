//
//  NYCSchoolServiceTests.swift
//  NYCSchoolsTests
//
//  Created by Sravanthi Kande on 5/17/23.
//

import Combine
import XCTest
@testable import NYCSchools

class NYCSchoolServiceTests: XCTestCase {
    
    var cancellables: Set<AnyCancellable> = []
    
    func testFetchSchools() {
        // Given
        let expectedSchools = MockSchoolService.mockResponse
        
        let mockSchoolService = MockSchoolService(expectedSchools: expectedSchools)
        let viewModel = SchoolViewModel(schoolService: mockSchoolService)
        let expectation = XCTestExpectation(description: "Fetch schools")
        
        // When
        viewModel.fetchSchools { error in
            XCTAssertNil(error)
            expectation.fulfill()
        }
        
        // Then
        wait(for: [expectation], timeout: 1.0)
        XCTAssertEqual(viewModel.schools, expectedSchools)
    }
}

class MockSchoolService: SchoolServiceProtocol {
    let expectedSchools: [SchoolInfo]
    init(expectedSchools: [SchoolInfo]) {
        self.expectedSchools = expectedSchools
    }
    
    func fetchSchools() -> AnyPublisher<[SchoolInfo], Error> {
        return Just(expectedSchools)
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
    }
}

extension MockSchoolService {
    static let mockResponse = [
        SchoolInfo(
            dbn: "01M292",
            schoolName: "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES",
            overviewParagraph: "Henry Street School for International Studies is a small college preparatory school located in Manhattan.",
            phoneNumber: "212-406-9411",
            schoolEmail: nil,
            website: "http://www.schools.nyc.gov/schoolportals/02/M292",
            extracurricularActivities: "School Based Leadership Team, Drama Club, Student Council",
            city: "New York",
            zip: "10002",
            finalgrades: "6-12",
            totalStudents: "323",
            schoolSports: "Basketball, Softball, Volleyball",
            readingScore: "355",
            mathScore: "404",
            writingScore: "363"),
        SchoolInfo(
            dbn: "01M448",
            schoolName: "UNIVERSITY NEIGHBORHOOD MIDDLE SCHOOL",
            overviewParagraph: "University Neighborhood Middle School is a small, nurturing, academically challenging community.",
            phoneNumber: "212-962-4341",
            schoolEmail: nil,
            website: "http://www.unmslearning.com",
            extracurricularActivities: "Theater, Dance, Visual Arts, Sports, Music, Photography",
            city: "New York",
            zip: "10002",
            finalgrades: "6-8",
            totalStudents: "294",
            schoolSports: "Cross Country, Basketball, Softball, Volleyball",
            readingScore: "383",
            mathScore: "423",
            writingScore: "366")]
}
