//
//  SchoolViewModelTests.swift
//  NYCSchoolsTests
//
//  Created by Sravanthi Kande on 5/17/23.
//

import Foundation
import Combine
import XCTest
@testable import NYCSchools

class SchoolViewModelTests: XCTestCase {
    
    var mockSchoolService: MockSchoolModelService!
    var viewModel: SchoolViewModel!
    
    override func setUp() {
        super.setUp()
        mockSchoolService = MockSchoolModelService()
        viewModel = SchoolViewModel(schoolService: mockSchoolService)
    }
    
    override func tearDown() {
        mockSchoolService = nil
        viewModel = nil
        super.tearDown()
    }
    
    func testFetchSchoolsSuccess() {
        // Given
        let mockSchools = MockSchoolService.mockResponse
        let mockService = MockSchoolModelService()
        mockService.mockSchools = mockSchools
        let viewModel = SchoolViewModel(schoolService: mockService)
        
        // When
        let expectation = XCTestExpectation(description: "Fetch schools")
        viewModel.fetchSchools { error in
            // Then
            XCTAssertNil(error)
            XCTAssertEqual(viewModel.schools, mockSchools)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1)
    }
    
    func testFetchSchoolsFailure() {
        // Given
        let mockService = MockSchoolModelService()
        mockService.shouldReturnError = true
        let viewModel = SchoolViewModel(schoolService: mockService)
        
        // When
        let expectation = XCTestExpectation(description: "Fetch schools")
        viewModel.fetchSchools { error in
            // Then
            XCTAssertNotNil(error)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1)
    }
}

class MockSchoolModelService: SchoolServiceProtocol {
    var mockSchools: [SchoolInfo]?
    var shouldReturnError = false
    
    func fetchSchools() -> AnyPublisher<[SchoolInfo], Error> {
        if shouldReturnError {
            return Fail(error: SchoolServiceError.noDataFound(NSError(domain: "", code: 0, userInfo: nil)))
                .eraseToAnyPublisher()
        } else if let mockSchools = mockSchools {
            return Just(mockSchools)
                .setFailureType(to: Error.self)
                .eraseToAnyPublisher()
        } else {
            fatalError("MockSchoolService should have either mock schools or an error")
        }
    }
}

