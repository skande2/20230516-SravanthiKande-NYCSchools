//
//  SchoolDetailViewControllerTests.swift
//  NYCSchoolsTests
//
//  Created by Sravanthi Kande on 5/17/23.
//

import XCTest
@testable import NYCSchools

class SchoolDetailViewControllerTests: XCTestCase {
    
    var sut: SchoolDetailViewController!
    
    override func setUpWithError() throws {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyboard.instantiateViewController(withIdentifier: "SchoolDetailViewController") as? SchoolDetailViewController
        sut.loadViewIfNeeded()
    }
    
    override func tearDownWithError() throws {
        sut = nil
    }
    
    func testUpdateUIWithSchool() {
        guard let school = MockSchoolService.mockResponse.first else {
            return XCTFail("schoolInfo should not be nil")
        }
        sut.school = school
        sut.updateUI()
        
        XCTAssertEqual(sut.schoolNameLabel.text, school.schoolName)
        XCTAssertEqual(sut.readingScoreLabel.attributedText?.string, "Reading Score: 355")
        XCTAssertEqual(sut.mathScoreLabel.attributedText?.string, "Math Score: 404")
        XCTAssertEqual(sut.writingScoreLabel.attributedText?.string, "Writing Score: 363")
        XCTAssertEqual(sut.extracurricularLabel.attributedText?.string, "Extracurricular Activities: School Based Leadership Team, Drama Club, Student Council")
        XCTAssertEqual(sut.finalgrades.attributedText?.string, "Final Grades: 6-12")
        XCTAssertEqual(sut.totalStudents.attributedText?.string, "Total Students: 323")
    }
    
    func testUpdateUIWithoutSchool() {
        sut.updateUI()
        
        XCTAssertEqual(sut.schoolNameLabel.text, "-")
        XCTAssertEqual(sut.readingScoreLabel.attributedText?.string, "-")
        XCTAssertEqual(sut.mathScoreLabel.attributedText?.string, "-")
        XCTAssertEqual(sut.writingScoreLabel.attributedText?.string, "-")
        XCTAssertEqual(sut.overviewLabel.attributedText?.string, "-")
        XCTAssertEqual(sut.extracurricularLabel.attributedText?.string, "-")
        XCTAssertEqual(sut.finalgrades.attributedText?.string, "-")
        XCTAssertEqual(sut.totalStudents.attributedText?.string, "-")
    }
}
