//
//  SchoolInfo.swift
//  NYCSchools
//
//  Created by Sravanthi Kande on 5/16/23.
//

import Foundation

struct SchoolInfo: Codable, Equatable {
    let dbn: String
    let schoolName: String?
    let overviewParagraph: String?
    let phoneNumber: String?
    let schoolEmail: String?
    let website: String?
    let extracurricularActivities: String?
    let city: String?
    let zip: String?
    let finalgrades: String?
    let totalStudents: String?
    let schoolSports: String?
    let readingScore: String?
    let mathScore: String?
    let writingScore: String?
    
    private enum CodingKeys: String, CodingKey {
        case dbn, website, city, zip
        case schoolName = "school_name"
        case overviewParagraph = "overview_paragraph"
        case phoneNumber = "phone_number"
        case schoolEmail = "school_email"
        case finalgrades = "finalgrades"
        case extracurricularActivities = "extracurricular_activities"
        case totalStudents = "total_students"
        case schoolSports = "school_sports"
        case readingScore = "sat_critical_reading_avg_score"
        case mathScore = "sat_math_avg_score"
        case writingScore = "sat_writing_avg_score"
    }
}
