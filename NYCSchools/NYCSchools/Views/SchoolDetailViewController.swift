//
//  SchoolDetailViewController.swift
//  NYCSchools
//
//  Created by Sravanthi Kande on 5/16/23.
//

import UIKit

class SchoolDetailViewController: UIViewController {
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var readingScoreLabel: UILabel!
    @IBOutlet weak var mathScoreLabel: UILabel!
    @IBOutlet weak var writingScoreLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var extracurricularLabel: UILabel!
    @IBOutlet weak var finalgrades: UILabel!
    @IBOutlet weak var totalStudents: UILabel!
    @IBOutlet weak var website: UILabel!
    @IBOutlet weak var schoolEmail: UILabel!
    @IBOutlet weak var cityZip: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!

    // The selected school for which the details need to be displayed
    var school: SchoolInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = Constants.detailVCTitle
        // Update the UI with the selected school details
        updateUI()
    }
    
    // Method to update the UI with the selected school details
    func updateUI() {
        guard let school = school else { return }
        
        schoolNameLabel.text = school.schoolName
        readingScoreLabel.attributedText = attributedText(withLabel: Constants.readingScore, value: school.readingScore)
        mathScoreLabel.attributedText = attributedText(withLabel: Constants.mathScore, value: school.mathScore)
        writingScoreLabel.attributedText = attributedText(withLabel: Constants.writingScore, value: school.writingScore)
        overviewLabel.attributedText = attributedText(withLabel: Constants.overview, value: school.overviewParagraph)
        extracurricularLabel.attributedText = attributedText(withLabel: Constants.activities, value: school.extracurricularActivities)
        finalgrades.attributedText = attributedText(withLabel: Constants.finalGrades, value: school.finalgrades)
        totalStudents.attributedText = attributedText(withLabel: Constants.totalStudents, value: school.totalStudents)
        website.attributedText = attributedText(withLabel: Constants.website, value: school.website)
        phoneNumber.attributedText = attributedText(withLabel: Constants.phone, value: school.phoneNumber)
        schoolEmail.attributedText = attributedText(withLabel: Constants.email, value: school.schoolEmail)
        cityZip.attributedText = attributedText(withLabel: Constants.city, value: "\(school.city ?? "-"), \(school.zip ?? "-")")
    }

    func attributedText(withLabel label: String, value: String?) -> NSAttributedString {
        let labelText = "\(label) \(value ?? "-")"
        return labelText.splitWithFonts()
    }
}
