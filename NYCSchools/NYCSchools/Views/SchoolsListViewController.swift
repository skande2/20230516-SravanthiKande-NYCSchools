//
//  SchoolsListViewController.swift
//  NYCSchools
//
//  Created by Sravanthi Kande on 5/16/23.
//

import UIKit

class SchoolsListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    private var loadingIndicator: UIActivityIndicatorView!

    var viewModel: SchoolViewModelProtocol = SchoolViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the title of the view controller
        title = Constants.listVCTitle
        tableView.register(SchoolListCell.self, forCellReuseIdentifier: Constants.schoolListCell)

        // Add loading indicator
        loadingIndicator = UIActivityIndicatorView(style: .medium)
        loadingIndicator.center = view.center
        view.addSubview(loadingIndicator)

        // Fetch schools list
        fetchSchools()
    }
    
    // Function to fetch the schools list
    private func fetchSchools() {
        // Show loading indicator
        loadingIndicator.startAnimating()
        

        viewModel.fetchSchools { [weak self] error in
            // Hide loading indicator
            self?.loadingIndicator.stopAnimating()

            if let error = error {
                // Show alert if there is an error while fetching schools
                let alertVC = UIAlertController(title: Constants.error, message: error.localizedDescription, preferredStyle: .alert)
                DispatchQueue.main.async {
                    self?.present(alertVC, animated: true)
                }
            } else {
                // Reload table view after fetching schools
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
            }
        }
    }
}


extension SchoolsListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.schools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.schoolListCell, for: indexPath) as! SchoolListCell
        let data = viewModel.schools[indexPath.row]
        // Configure the cell with the school info
        cell.configure(with: data)
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let school = viewModel.schools[indexPath.row]
        
        // Create a new instance of the detail view controller
        let storyboard = UIStoryboard(name: Constants.main, bundle: nil)
        guard let schoolDetailViewController = storyboard.instantiateViewController(withIdentifier: Constants.detailVCIdentifier) as? SchoolDetailViewController else {
            return
        }

        schoolDetailViewController.school = school
        // Push the detail view controller onto the navigation stack

        self.navigationController?.pushViewController(schoolDetailViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
