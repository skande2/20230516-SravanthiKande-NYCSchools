//
//  SchoolViewModel.swift
//  NYCSchools
//
//  Created by Sravanthi Kande on 5/16/23.
//

import Foundation
import Combine

protocol SchoolViewModelProtocol {
    var schools: [SchoolInfo] { get }
    func fetchSchools(completion: @escaping (Error?) -> Void)
}

class SchoolViewModel: SchoolViewModelProtocol {
    private let schoolService: SchoolServiceProtocol
    private(set) var schools: [SchoolInfo] = []
    private var cancellables = Set<AnyCancellable>()
    
    // Initializer to inject schoolService
    init(schoolService: SchoolServiceProtocol = SchoolService()) {
        self.schoolService = schoolService
    }

    // Function to fetch schools data
    func fetchSchools(completion: @escaping (Error?) -> Void) {
        schoolService.fetchSchools() // Fetch schools using schoolService object
            .receive(on: DispatchQueue.main) // Receive the fetched data on main thread
            .sink(receiveCompletion: { completionResult in // Handle the completion of the publisher
                if case let .failure(error) = completionResult { // Handle any error received
                    completion(error) // Call completion with received error
                }
            }, receiveValue: { schools in // Handle the received value (schools data)
                self.schools = schools // Store the received schools data
                completion(nil) // Call completion with no error
            })
            .store(in: &cancellables) // Store the subscriber in cancellables for cancellation if needed
    }
    
    deinit {
        cancellables.forEach { $0.cancel() }
    }
}
