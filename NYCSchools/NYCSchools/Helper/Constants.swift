//
//  Constants.swift
//  NYCSchools
//
//  Created by Sravanthi Kande on 5/17/23.
//

import Foundation

struct Constants {
    static let listVCTitle = "Schools List"
    static let detailVCTitle = "School Details"
    static let schoolListCell = "SchoolListCell"
    static let readingScore = "Reading Score:"
    static let mathScore = "Math Score:"
    static let writingScore = "Writing Score:"
    static let overview = "Overview:"
    static let activities = "Extracurricular Activities:"
    static let finalGrades = "Final Grades:"
    static let totalStudents = "Total Students:"
    static let website = "Website:"
    static let phone = "Phone:"
    static let email = "School Email:"
    static let city = "City:"
    static let error = "Error"
    static let detailVCIdentifier = "SchoolDetailViewController"
    static let main = "Main"
}
