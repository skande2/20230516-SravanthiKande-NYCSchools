//
//  Extensions.swift
//  NYCSchools
//
//  Created by Sravanthi Kande on 5/16/23.
//

import UIKit

extension String {
    func splitWithFonts(using separator: String = ":",
                        boldFont: UIFont = UIFont.boldSystemFont(ofSize: 14),
                        regularFont: UIFont = UIFont.systemFont(ofSize: 14)) -> NSAttributedString {
        let components = self.components(separatedBy: separator)
        let boldAttributes: [NSAttributedString.Key: Any] = [.font: regularFont]
        let regularAttributes: [NSAttributedString.Key: Any] = [.font: boldFont]
        let attributedString = NSMutableAttributedString()
        components.enumerated().forEach { index, component in
            let convertString = (index == 0) ? (component + ":") : component
            let attributedComponent = NSAttributedString(string: convertString, attributes: index % 2 == 0 ? regularAttributes : boldAttributes)
            attributedString.append(attributedComponent)
        }
        return attributedString
    }
}
