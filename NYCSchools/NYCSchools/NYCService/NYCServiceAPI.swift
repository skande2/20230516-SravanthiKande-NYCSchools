//
//  NYCServiceAPI.swift
//  NYCSchools
//
//  Created by Sravanthi Kande on 5/16/23.
//

import Foundation
import Combine

enum SchoolServiceError: Error {
    case invalidURL
    case noDataFound(Error)
}

protocol SchoolServiceProtocol {
    func fetchSchools() -> AnyPublisher<[SchoolInfo], Error>
}

class SchoolService: SchoolServiceProtocol {
    let firstEndpoint = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    let secondEndpoint = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    let session: URLSession
    
    init(session: URLSession = URLSession.shared) {
        self.session = session
    }
    
    func fetchSchools() -> AnyPublisher<[SchoolInfo], Error> {
        // Fetch data from both endpoints and combine them into a single publisher of [SchoolInfo]
        let firstPublisher = fetchData(from: firstEndpoint) as AnyPublisher<[SchoolInfo], Error>
        let secondPublisher = fetchData(from: secondEndpoint) as AnyPublisher<[SchoolInfo], Error>
        
        return Publishers.CombineLatest(firstPublisher, secondPublisher)
            .map { firstApiResponse, secondApiResponse in
                // Combine the data from both endpoints by matching schools' dbn and
                // creating a new SchoolInfo object that has all the data
                self.combineApiResponses(firstApiResponse: firstApiResponse, secondApiResponse: secondApiResponse)
            }
            .eraseToAnyPublisher()
    }
    
    // Fetch data from a given URL and return a publisher of type [T]
    private func fetchData<T: Codable>(from url: String) -> AnyPublisher<[T], Error> {
        guard let url = URL(string: url) else {
            return Fail(error: SchoolServiceError.invalidURL)
                .eraseToAnyPublisher()
        }
        
        return session.dataTaskPublisher(for: url)
            .map(\.data)
            .decode(type: [T].self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
    
    // Combine the data from both endpoints by matching schools' dbn and creating a new SchoolInfo object that has all the data
    // It took some time to develop and implement the logic to properly combine the responses from two different endpoints in this method.
    private func combineApiResponses(firstApiResponse: [SchoolInfo], secondApiResponse: [SchoolInfo]) -> [SchoolInfo] {
        return firstApiResponse.map { school in
            let matchingSATScore = secondApiResponse.first(where: { $0.dbn == school.dbn })
            
            return SchoolInfo(
                dbn: school.dbn,
                schoolName: school.schoolName,
                overviewParagraph: school.overviewParagraph,
                phoneNumber: school.phoneNumber,
                schoolEmail: school.schoolEmail,
                website: school.website,
                extracurricularActivities: school.extracurricularActivities,
                city: school.city,
                zip: school.zip,
                finalgrades: school.finalgrades,
                totalStudents: school.totalStudents,
                schoolSports: school.schoolSports,
                readingScore: matchingSATScore?.readingScore,
                mathScore: matchingSATScore?.mathScore,
                writingScore: matchingSATScore?.writingScore
            )
        }
    }
}
